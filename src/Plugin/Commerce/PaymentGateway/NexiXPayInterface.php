<?php

namespace Drupal\commerce_nexi_xpay\Plugin\Commerce\PaymentGateway;

/**
 * Provides the interface for the Nexi XPay payment gateway.
 */
interface NexiXPayInterface {

  /**
   * Get the configured Nexi XPay Alias.
   *
   * @return string
   *   The Nexi XPay Alias.
   */
  public function getAlias();

  /**
   * Get the configured Nexi XPay Secret key.
   *
   * @return string
   *   The Nexi XPay Secret key.
   */
  public function getSecretKey();

  /**
   * Get the Nexi XPay base url to use for API requests based on the mode.
   *
   * @return string
   *   The Nexi XPay URL.
   */
  public function getBaseUrl();

}
