<?php

namespace Drupal\commerce_nexi_xpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Nexi XPay payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "nexi_xpay",
 *   label =  @Translation("Nexi XPay"),
 *   display_label = @Translation("Nexi XPay"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_nexi_xpay\PluginForm\NexiXPayForm",
 *   },
 *   payment_method_types = {"credit_card"},
 * )
 */
class NexiXPay extends OffsitePaymentGatewayBase implements NexiXPayInterface {

  const NEXI_XPAY_LIVE_URL = 'https://ecommerce.nexi.it/ecomm/ecomm/DispatcherServlet';

  const NEXI_XPAY_STAGING_URL = 'https://int-ecommerce.nexi.it/ecomm/ecomm/DispatcherServlet';

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function getAlias() {
    return $this->configuration['alias'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSecretKey() {
    return $this->configuration['secret_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    if ($this->getMode() == 'live') {
      return self::NEXI_XPAY_LIVE_URL;
    }
    else {
      return self::NEXI_XPAY_STAGING_URL;
    }
  }

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerChannelFactoryInterface $logger_channel_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->logger = $logger_channel_factory->get('commerce_nexi_xpay');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'alias' => '',
      'secret_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['alias'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alias'),
      '#default_value' => $this->configuration['alias'],
      '#required' => TRUE,
      '#maxlength' => 30,
      '#size' => 60,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 60,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors() && $form_state->isSubmitted()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['alias'] = $values['alias'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['mode'] = $values['mode'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['alias'] = $values['alias'];
      $this->configuration['secret_key'] = $values['secret_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $secret_key = $this->getSecretKey();

    // Verification of the mandatory parameters for the calculation of the MAC.
    $required_params = ['codTrans',
      'esito',
      'importo',
      'divisa',
      'data',
      'orario',
      'codAut',
      'mac',
    ];
    foreach ($required_params as $param) {
      if (!$request->query->has($param)) {
        throw new PaymentGatewayException('Missing parameter: ' . $param);
      }
    }

    // Calcolo MAC con i parametri di ritorno.
    $mac_calculated = sha1('codTrans=' . $request->query->get('codTrans') .
      'esito=' . $request->query->get('esito') .
      'importo=' . $request->query->get('importo') .
      'divisa=' . $request->query->get('divisa') .
      'data=' . $request->query->get('data') .
      'orario=' . $request->query->get('orario') .
      'codAut=' . $request->query->get('codAut') .
      $secret_key
    );

    // Verifico corrispondenza tra MAC calcolato e MAC di ritorno.
    if ($mac_calculated != $request->query->get('mac')) {
      throw new PaymentGatewayException('Invalid MAC');
    }

    if ($request->query->get('esito') == 'OK') {
      $message = $this->t('Payment was processed.');
      $this->messenger()->addMessage($message);
    }
    else {
      $this->logger->warning('Error: ' . $request->query->get('messaggio'));
      throw new PaymentGatewayException('The transaction was refused.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $secret_key = $this->getSecretKey();

    $logger = \Drupal::logger('commerce_nexi_xpay');
    $logger->debug('e-Commerce payment response: <pre>@body</pre>', [
      '@body' => var_export($request->request->all(), TRUE),
    ]);
    /*
    $logger->debug('e-Commerce payment response: <pre>@body</pre>', [
    '@body' => json_encode($request->request->all()),
    ]);
     */

    // Verification of the mandatory parameters for the calculation of the MAC.
    $required_params = ['codTrans',
      'esito',
      'importo',
      'divisa',
      'data',
      'orario',
      'codAut',
      'mac',
    ];

    foreach ($required_params as $param) {
      if (!$request->request->has($param)) {
        $response = new Response();
        $response->setContent('Missing parameter: ' . $param);
        $response->setStatusCode(500, '500 Internal Server Error');
        return $response;
      }
    }

    // Mac calculation with return parameters.
    $mac_calculated = sha1('codTrans=' . $request->request->get('codTrans') .
      'esito=' . $request->request->get('esito') .
      'importo=' . $request->request->get('importo') .
      'divisa=' . $request->request->get('divisa') .
      'data=' . $request->request->get('data') .
      'orario=' . $request->request->get('orario') .
      'codAut=' . $request->request->get('codAut') .
      $secret_key
    );

    // Check correspondence between calculated MAC and return MAC.
    if ($mac_calculated != $request->request->get('mac')) {
      $message = 'MAC Error: ' . $mac_calculated . ' does not correspond to ' . $request->request->get('mac');

      $this->logger->alert($message);

      $response = new Response();
      $response->setContent($message);
      $response->setStatusCode(500, '500 Internal Server Error');
      return $response;
    }

    // If there are no errors I manage the outcome parameter.
    $order_id = explode('-', $request->request->get('codTrans'));
    $order_id = $order_id[0];
    $order = Order::load($order_id);

    // $payment = $this->entityTypeManager->getStorage('commerce_payment')->load($request->request->get('codTrans'));
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->entityId,
      'order_id' => $order->id(),
      // 'remote_state' => $transactionData['status'],
    ]);
    $payment->save();
    $payment->setRemoteState($request->request->get('codiceEsito'));

    if ($request->request->get('esito') == 'OK') {
      $payment->set('state', 'completed');
      $payment->setRemoteId($request->request->get('codAut'));
    }
    else {
      $payment->setState('authorization_voided');
    }
    $payment->save();
  }

}
