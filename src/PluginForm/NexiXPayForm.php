<?php

namespace Drupal\commerce_nexi_xpay\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class NexiXPayForm.
 *
 * @package Drupal\commerce_nexi_xpay\PluginForm
 */
class NexiXPayForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_nexi_xpay\Plugin\Commerce\PaymentGateway\NexiXPay $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    // Save the payment entity so that we can get its ID.
    // $payment->save();

    // Alias and secret key.
    $alias = $payment_gateway_plugin->getAlias();
    $secret_key = $payment_gateway_plugin->getSecretKey();

    $redirect_url = $payment_gateway_plugin->getBaseUrl();
    $notify_url = $payment_gateway_plugin->getNotifyUrl();
    $urlpost = $notify_url->toString();

    $order = $payment->getOrder();
    $cod_trans = $order->id() . '-' . date('YmdHis');
    // $cod_trans = $payment->id();
    $divisa = "EUR"; /* $amount->getCurrencyCode(); */
    $amount = $payment->getAmount();
    $amount = (int) $amount->multiply(100)->getNumber();
    // TODO: currency conversion.
    /* $amount->convert($currency_code, $rate = 1) */

    // MAC calculation.
    $mac = sha1('codTrans=' . $cod_trans . 'divisa=' . $divisa . 'importo=' . $amount . $secret_key);

    // Mandatory parameters.
    $data = [
      'alias' => $alias,
      'importo' => $amount,
      'divisa' => $divisa,
      'codTrans' => $cod_trans,
      'url' => $form['#return_url'],
      'url_back' => $form['#cancel_url'],
      'mac' => $mac,
      'descrizione' => t('Order @order_id', ['@order_id' => $order->id()]),
      'urlpost' => $urlpost,
    ];

    return $this->buildRedirectForm($form, $form_state, $redirect_url, $data, BasePaymentOffsiteForm::REDIRECT_POST);
  }

}
